#include <Windows.h>
#include <TlHelp32.h>
#include <iostream>

DWORD FindProcessId(const std::wstring& processName)
{
    // Take a snapshot of all processes in the system.
    HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

    PROCESSENTRY32W processEntry;
    processEntry.dwSize = sizeof(PROCESSENTRY32W);

    if (!Process32FirstW(snapshot, &processEntry))
    {
        CloseHandle(snapshot);
        std::cout << "Failed to gather information on system processes! Error: " << GetLastError() << std::endl;
        return 0;
    }

    do
    {
        if (std::wstring(processEntry.szExeFile) == processName)
        {
            CloseHandle(snapshot);
            return processEntry.th32ProcessID;
        }
    } while (Process32NextW(snapshot, &processEntry));

    CloseHandle(snapshot);
    return 0;
}

BOOL InjectDLL(DWORD processID, const char* dllPath) {
    HANDLE hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, processID);
    if (hProcess == NULL) {
        std::cout << "Could not open process" << std::endl;
        return FALSE;
    }

    void* pDllPath = VirtualAllocEx(hProcess, 0, strlen(dllPath) + 1, MEM_COMMIT, PAGE_READWRITE);
    WriteProcessMemory(hProcess, pDllPath, (void*)dllPath, strlen(dllPath) + 1, 0);

    FARPROC hLoadLibraryA = GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryA");
    HANDLE hThread = CreateRemoteThread(hProcess, 0, 0, (LPTHREAD_START_ROUTINE)hLoadLibraryA, pDllPath, 0, 0);

    if (hThread == NULL) {
        std::cout << "Could not create thread" << std::endl;
        return FALSE;
    }


    CloseHandle(hProcess);
    return TRUE;
}

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cout << "Please provide the path to the DLL as a command line argument." << std::endl;
        return 1;
    }

    const char* dllPath = argv[1];  //DLL path from command line argument

    while (true)  // Enter an infinite loop
    {
        DWORD processId = FindProcessId(L"bugMove.exe");  // process name

        if (processId != 0)  // If processId is not 0, i.e., the process is running
        {
            BOOL result = InjectDLL(processId, dllPath);
            if (result)
            {
                std::cout << "DLL injected successfully!" << std::endl;
            }
            else
            {
                std::cout << "DLL injection failed!" << std::endl;
            }
        }
        else
        {
            std::cout << "Process not found. Make sure your target process is running." << std::endl;
        }

        // Sleep for 5 seconds
        Sleep(5000);
    }

    return 0;
}
