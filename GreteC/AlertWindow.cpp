#include <Windows.h>
#include "peb_lookup.h"

int main()
{
    wchar_t kernel32dll[] = { 'k','e','r','n','e','l','3','2','.','d','l','l',0 };
    char LoadLibraryA[] = { 'L','o','a','d','L','i','b','r','a','r','y','A',0 };
    char GetProcAddress[] = { 'G','e','t','P','r','o','c','A','d','d','r','e','s','s',0 };
    char GetSystemTime[] = { 'G','e','t','S','y','s','t','e','m','T','i','m','e',0 };
    char user32dll[] = { 'u','s','e','r','3','2','.','d','l','l',0 };
    char MessageBoxW[] = { 'M','e','s','s','a','g','e','B','o','x','W',0 };
    wchar_t CurrentTime[] = { 'C','u','r','r','e','n','t',' ','T','i','m','e',0 };

    LPVOID base = get_module_by_name(kernel32dll);
    if (!base) {
        return 1;
    }

    LPVOID load_lib = get_func_by_name((HMODULE)base, LoadLibraryA);
    if (!load_lib) {
        return 2;
    }

    LPVOID get_proc = get_func_by_name((HMODULE)base, GetProcAddress);
    if (!get_proc) {
        return 3;
    }

    HMODULE(WINAPI * _LoadLibraryA)(LPCSTR lpLibFileName) = (HMODULE(WINAPI*)(LPCSTR))load_lib;
    FARPROC(WINAPI * _GetProcAddress)(HMODULE hModule, LPCSTR lpProcName)
        = (FARPROC(WINAPI*)(HMODULE, LPCSTR)) get_proc;

    LPVOID u32_dll = _LoadLibraryA(user32dll);

    int (WINAPI * _MessageBoxW)(
        _In_opt_ HWND hWnd,
        _In_opt_ LPCWSTR lpText,
        _In_opt_ LPCWSTR lpCaption,
        _In_ UINT uType) = (int (WINAPI*)(
            _In_opt_ HWND,
            _In_opt_ LPCWSTR,
            _In_opt_ LPCWSTR,
            _In_ UINT)) _GetProcAddress((HMODULE)u32_dll, MessageBoxW);

    void (WINAPI * _GetSystemTime)(LPSYSTEMTIME lpSystemTime) = (void(WINAPI*)(LPSYSTEMTIME))
        _GetProcAddress((HMODULE)base, GetSystemTime);

    if (_MessageBoxW == NULL || _GetSystemTime == NULL) return 4;

    // Get current time
    SYSTEMTIME st;
    _GetSystemTime(&st);

    wchar_t buffer[255];
    SecureZeroMemory(buffer, sizeof(buffer));
    buffer[0] = L'0' + st.wYear / 1000 % 10;
    buffer[1] = L'0' + st.wYear / 100 % 10;
    buffer[2] = L'0' + st.wYear / 10 % 10;
    buffer[3] = L'0' + st.wYear % 10;
    buffer[4] = L'-';
    buffer[5] = L'0' + st.wMonth / 10;
    buffer[6] = L'0' + st.wMonth % 10;
    buffer[7] = L'-';
    buffer[8] = L'0' + st.wDay / 10;
    buffer[9] = L'0' + st.wDay % 10;
    buffer[10] = L' ';
    buffer[11] = L'0' + st.wHour / 10;
    buffer[12] = L'0' + st.wHour % 10;
    buffer[13] = L':';
    buffer[14] = L'0' + st.wMinute / 10;
    buffer[15] = L'0' + st.wMinute % 10;
    buffer[16] = L':';
    buffer[17] = L'0' + st.wSecond / 10;
    buffer[18] = L'0' + st.wSecond % 10;
    buffer[19] = L'\0';

    _MessageBoxW(0, buffer, CurrentTime, MB_OK);

    return 0;
}
