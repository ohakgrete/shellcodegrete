# ShellcodeGrete

## Getting started with my project
I have added a report titled 'RaportGrete' and a screenshot 'Screenshot (3) as proof of the successful injection process. NOTE: x86 architecture (32 bit) used in my case.
Solutions I provide: GreteC (files: AlertWindow.cpp, peb_lookup.h, AlertWindow.asm, AlertWindow.exe, AlertWindow.obj, AlertWindow.bin), DllGrete (files: DllGrete.dll, dllmain.cpp), InjectionProcess(files:InjectionProcess.cpp, InjectionProcess.exe), bugMove.exe (just simple console app where the word „bug“ is moving around), injector.cpp (in case you want to provide hardcoded DLL path).

## My code execution

1) Method1,  simple way, just run 2 .exe files
- Download 3 things: InjectionProcess.exe, DllGrete.dll, bugMove.exe
- Open cmd and move to the directory where InjectionProcess.exe is located
- Write into cmd: InjectionProcess.exe path\to\DllGrete.dll
- Now you can see the console, what is printing if the DLL injection is done already or not (like a status console)
- Run bugMove.exe and you can see a word „bug“ moving around the console, and alert shows up almost immediately, so you can close the alert and „bug“ keeps moving like it should.

2) Method2, I can provide similar .cpp file like InjectionProcess.cpp where you can hardcode DllGrete.dll location if you need it in that way (it is named as injector.cpp).
- Hardcode there DllGrete.dll location (in injector.cpp)
- Run the injector.cpp as you would like, console shows up what is printing if the DLL injection is done already or not (like a status console)
- Run bugMove.exe and you can see a word „bug“ moving around the console, and alert shows up almost immediately, so you can close the alert and „bug“ keeps moving like it should.




