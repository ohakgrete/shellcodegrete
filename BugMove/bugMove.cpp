#include <iostream>
#include <cstdlib>
#include <ctime>
#include <windows.h>

const int width = 20;
const int height = 20;

void gotoxy(int x, int y) {
    COORD coord;
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

void Draw() {
    system("cls");  // clear the screen
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            if (i == 0 || i == height - 1 || j == 0 || j == width - 1)
                std::cout << "#";
            else
                std::cout << " ";
        }
        std::cout << "\n";
    }
}

void placeBug() {
    int x = rand() % (width - 2) + 1;  // Generate random x position within border
    int y = rand() % (height - 2) + 1;  // Generate random y position within border
    gotoxy(x, y);
    std::cout << "bug";
}

int main() {
    srand(static_cast<unsigned>(time(0)));  // seed the random number generator
    while (true) {
        Draw();
        placeBug();
        Sleep(1000);  // pause for a second
    }
    return 0;
}
